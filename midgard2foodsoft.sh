#!/usr/bin/zsh -x 
#convert the ods's to csv, semicolon seperated
#rm *.csv
unoconv -f csv -e FilterOptions="59,34,0,1" *'Frischeliste'*'.ods' &&
unoconv -f csv -e FilterOptions="59,34,0,1" *"BioCompany-Angebot.ods"&&
unoconv -f csv -e FilterOptions="59,34,0,1" *'Preisliste'* &&
mv *'Frischeliste'*'.csv' frische.csv
mv *'Preisliste'*'.csv' generell.csv
mv *"BioCompany-Angebot.csv" biocomp.csv
#clean start
########################
rm midgardimport.csv
## remove lines not staring with artikelnr
sed -e '/^\([0-9].*\)/!d' biocomp.csv > biocomp_products
sed -e '/^\([0-9].*\)/!d' generell.csv > general_products
sed -e '/^\([0-9].*\)/!d' frische.csv  > frische_products
rm *.csv

while  IFS= read -r PRODUCT
do

	# have: 1 artnr  | 2 Name | 3 quality | 4 origin | 5 gebinde x unit | 6 netto 
	# want: status | artnr | Name | quality | producer | origin | unit | netto | mwst | pfand | gebinde | none | none | category 
	# thus:  ;1;2;3;;4;geb[2];6;Mwst;"0"; geb[1];;; "Haltbares"
		
	echo $PRODUCT | awk -F\; '{ split($5,geb," x "); split($6,netto," €"); printf(";%s;%s;%s;;%s;%s;%s;7;0;%s;;;Haltbares\n",$1,$2,$3,$4,geb[2],netto[1],geb[1]);}' | sed -e "s/\" //" | sed -e "s/\"//g"  >> midgardimport_BC.csv

done < biocomp_products
########################
rm midgardimport_frische.csv
while IFS= read -r PRODUCT
do

	# have: 1 artnr | 2 producer | 3 name | 4 na | 5 na | 6 na | 7 na | 8 na | 9 na | 10 na | 11 quality 12 origin | 13 gebinde | 14 unit  | 15 netto
	# want: status | artnr | Name | quality | producer | origin | unit | netto | mwst | pfand | gebinde | none | none | category 
		
	echo $PRODUCT | awk -F\; '{  printf(";%s;%s;%s;%s;%s;%s;%s;7;0;%s;;;Gemuese\n",$1,$3,$11,$2,$12,$14,$15,$13i);}' | sed -e "s/\" //" | sed -e "s/\"//g" | sed -e "s/x //g" | sed -e "s/ €//"  >> midgardimport_frische.csv

done < frische_products
########################
rm midgarimport_general.csv
while IFS= read -r PRODUCT
do
	# have: 1artnr | 2 Name | 3 Na | 4 Name2 | 5 producer | 6 quality | 7 origin | 8 gebinde | 9 Na | 10 unit | 11 netto 
	# want: status | artnr | Name | quality | producer | origin | unit | netto | mwst | pfand | gebinde | none | none | category 
	echo $PRODUCT | awk -F\; '{ printf(";%s;%s%s;%s;%s;%s;%s;%s;7;0;%s;;;Generell\n",$1,$2,$4,$6,$5,$7,$10,$11,$8);}' | sed -e "s/\" //" | sed -e "s/\"//g" | sed -e "s/ \+/ /g"  >> midgardimport_general.csv
done < general_products

## foodsoft timeouts if imports are too big, so split int chungs of 1000 lines
split -l 1000 midgardimport_general.csv
mmv "xa*" "midgardimport_generell_#1.csv"
rm  midgardimport_general.csv
rm frische_products
rm general_products
rm biocomp_products

#rm *.csv
