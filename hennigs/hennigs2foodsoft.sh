#!/bin/bash -x


# little script to scratch the website, get the three recent pdfs convert and convert them to csv format to import in foodsoft

# lear all pdf's

rm *.pdf*
curl http://www.hennig-biogaertnerei-glasatelier.de/index.php/angebote-der-woche-main-2 > site.html
rg Anbau.pdf site.html | sed -e "s/.*\(http.*Anbau\.pdf\)\".*/\1/" | xargs wget
mv *Anbau.pdf Eigener_Anbau.pdf
rg Anbau.pdf site.html | sed -e "s/.*\(http.*Backwaren\.pdf\)\".*/\1/g" | xargs wget
mv Zukauf*.pdf Backwaren.pdf
# now he following goes for the second occurence of Zukauf, by destroying the fitst occurence
rg Anbau.pdf site.html | sed -e "s/Backwaren//g" | sed -e "s/.*\(http.*Zukauf.*pdf\)\".*/\1/g" | xargs wget
mv Zukauf* Zukauf.pdf


# now extract tables of the pdfs using a tool called tabbula-java
java -jar tabula-java/target/tabula-1.0.3-jar-with-dependencies.jar --pages all -o backwaren.csv Backwaren.pdf 
sed -e '/^1.*/!d' backwaren.csv > backwaren_clean.csv

java -jar tabula-java/target/tabula-1.0.3-jar-with-dependencies.jar --pages all -o zukauf.csv Zukauf.pdf 
# honestly lets not list these products as they are not local anyway

java -jar tabula-java/target/tabula-1.0.3-jar-with-dependencies.jar --pages all -o eigener_anbau.csv Eigener_Anbau.pdf 
# following misses "1 500g gemischte flocken but who cares...
sed -e '/^[01]/!d' eigener_anbau.csv eigener_anau_clean.csv

# it is left to clean up alignment and awk them into the right order.
